package com.example.onboarding

data class OnBoardingItem(
    val onboardingImage : Int,
    val title : String,
    val description : String
)
